﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbit : MonoBehaviour {

	public GameObject objectToOrbit;
	public float rotationSpeed = 80.0f;
	public float orbitSpeed = 10.0f;
	public float radious = 5.0f;

	private float angle = 0.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//rotate it self
		this.transform.Rotate (Vector3.up * Time.deltaTime * rotationSpeed);

		//orbit target object
		angle += orbitSpeed * Time.deltaTime;
		Vector3 offset = new Vector3 (Mathf.Sin (angle), 0, Mathf.Cos (angle)) * radious;
		transform.position = objectToOrbit.transform.position + offset;
	}
}
